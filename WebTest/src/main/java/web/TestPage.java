package WEB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TestPage {
    private WebDriver driver;

    @FindBy(xpath = "//span[text()=\"Home\"]")
    private WebElement button_home;

    @FindBy(xpath = "//a[@href='/about']")
    private WebElement button_about;
    @FindBy(xpath = "//a[@href='/contact']")
    private WebElement button_contact;
    @FindBy(xpath = "//li[contains(@class, 'surface')]")
    private WebElement button_hello;
    @FindBy(xpath = "//span[contains(text(), 'Profile')]]")
    private WebElement button_menu_profile;
    @FindBy(xpath = "//span[contains(text(), 'Login')]]")
    private WebElement button_menu_login;
    @FindBy(xpath = "//span[contains(text(), 'Logout')]]")
    private WebElement button_menu_logout;
    //li[contains(@class, 'surface')]/a/text()

    @FindBy(xpath = "//input[@type='text']")
    private WebElement input_username;
    @FindBy(xpath = "//input[@type='password']")
    private WebElement input_password;
    @FindBy(xpath = "//button[@form='login']")
    private WebElement button_login;

    @FindBy(xpath = "//h1")
    private WebElement error_code;
    @FindBy(xpath = "//p")
    private WebElement error_message;


    @FindBy(xpath = "//*[@id=\"app\"]/main/div/div[1]/h1")
    private WebElement title_Blog;

    @FindBy(xpath = "//*[@id=\"create-btn\"]")
    private WebElement button_create;
    @FindBy(xpath = "//i[contains(text(), 'sort')][2]/parent::button")
    private WebElement button_order;
    @FindBy(xpath = "//button[contains(@class, 'switch')]")
    private WebElement button_switch_not_my_posts;

    @FindBy(xpath = "//p")
    private WebElement message;

    @FindBy(xpath = "//a[contains(@class, 'post')][1]")
    private WebElement post1;
    @FindBy(xpath = "//a[contains(@class, 'post')][1]/img")
    private WebElement post1_img;
    @FindBy(xpath = "//a[contains(@class, 'post')][1]/h2")
    private WebElement post1_title;
    @FindBy(xpath = "//a[contains(@class, 'post')][1]/div")
    private WebElement post1_description;

    @FindBy(xpath = "//a[contains(@class, 'post')][2]")
    private WebElement post2;
    @FindBy(xpath = " //a[contains(@class, 'post')][2]/img")
    private WebElement post2_img;
    @FindBy(xpath = "//a[contains(@class, 'post')][2]/h2")
    private WebElement post2_title;
    @FindBy(xpath = "//a[contains(@class, 'post')][2]/div")
    private WebElement post2_description;

    @FindBy(xpath = "//a[contains(@class, 'post')][3]")
    private WebElement post3;
    @FindBy(xpath = " //a[contains(@class, 'post')][3]/img")
    private WebElement post3_img;
    @FindBy(xpath = "//a[contains(@class, 'post')][3]/h2")
    private WebElement post3_title;
    @FindBy(xpath = "//a[contains(@class, 'post')][3]/div")
    private WebElement post3_description;

    @FindBy(xpath = "//a[contains(@class, 'post')][4]")
    private WebElement post4;
    @FindBy(xpath = " //a[contains(@class, 'post')][4]/img")
    private WebElement post4_img;
    @FindBy(xpath = "//a[contains(@class, 'post')][4]/h2")
    private WebElement post4_title;
    @FindBy(xpath = "//a[contains(@class, 'post')][4]/div")
    private WebElement post4_description;

    @FindBy(xpath = "//a[contains(text(), 'Previous Page')]")
    private WebElement button_previous_page;
    @FindBy(xpath = "//a[contains(text(), 'Next Page')]")
    private WebElement button_next_page;


    public TestPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WEB.TestPage loginUser() {
        input_username.sendKeys("Mikhaylova");
        input_password.sendKeys("08fbea9695");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginValid1() {
        input_username.sendKeys("1990");
        input_password.sendKeys("dc513ea4fb");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginMinSymbol() {
        input_username.sendKeys("MAV");
        input_password.sendKeys("977a916a31");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginMaxSymbol() {
        input_username.sendKeys("000000000000000000001");
        input_password.sendKeys("ce242f7923");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginInactive() {
        input_username.sendKeys("ka");
        input_password.sendKeys("2c68e1d508");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginRus() {
        input_username.sendKeys("Михайлова");
        input_password.sendKeys("b4089ef972");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginSpecSymbol() {
        input_username.sendKeys("ⓒ");
        input_password.sendKeys("75848fde84");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginlh() {
        input_username.sendKeys("na");
        input_password.sendKeys("6ec66e124f");
        button_login.click();
        return this;
    }

    public WEB.TestPage login21() {
        input_username.sendKeys("000000000000000000001");
        input_password.sendKeys("ce242f7923");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginestudianteNonValid() {
        input_username.sendKeys("Mikhaylova");
        input_password.sendKeys("нет");
        button_login.click();
        return this;
    }

    public WEB.TestPage loginWithoutLoginAndPassword() {
        input_username.sendKeys("");
        input_password.sendKeys("");
        button_login.click();
        return this;
    }

    public WEB.TestPage clickHome() {
        button_home.click();
        return this;
    }

    public WEB.TestPage switchToNotMtPosts() {
        button_switch_not_my_posts.click();
        return this;
    }

    public String getTitle_Blog() {
        return title_Blog.getText();
    }

    public WEB.TestPage nextPage() {
        button_next_page.click();
        return this;
    }

    public WEB.TestPage previousPage() {
        button_previous_page.click();
        return this;
    }

    public WebElement getPost1_title() {
        return post1_title;
    }

    public WebElement getPost2_title() {
        return post2_title;
    }

    public WebElement getPost3_title() {
        return post3_title;
    }

    public WebElement getPost4_title() {
        return post4_title;
    }

    public WebElement getPost1_description() {
        return post1_description;
    }

    public WebElement getPost2_description() {
        return post2_description;
    }

    public WebElement getPost3_description() {
        return post3_description;
    }

    public WebElement getPost4_description() {
        return post4_description;
    }

    public WebElement getPost1() {
        return post1;
    }

    public WebElement getPost2() {
        return post2;
    }

    public WebElement getPost3() {
        return post3;
    }

    public WebElement getPost4() {
        return post4;
    }

    public String getPost1_img_src() {
        return post1_img.getAttribute("src").toString();
    }

    public String getPost2_img_src() {
        return post2_img.getAttribute("src").toString();
    }

    public String getPost3_img_src() {
        return post3_img.getAttribute("src").toString();
    }

    public String getPost4_img_src() {
        return post4_img.getAttribute("src").toString();
    }

    public WebElement getPost1_img() {
        return post1_img;
    }

    public WebElement getPost2_img() {
        return post2_img;
    }

    public WebElement getPost3_img() {
        return post3_img;
    }

    public WebElement getPost4_img() {
        return post4_img;
    }

    public double getImg1AspectRatio() {
        double height = getPost1_img().getSize().height;
        double width = getPost1_img().getSize().width;
        double aspectRatio = height / width;
        return aspectRatio;
    }

    public double getImg2AspectRatio() {
        double height = getPost2_img().getSize().height;
        double width = getPost2_img().getSize().width;
        double aspectRatio = height / width;
        return aspectRatio;
    }

    public double getImg3AspectRatio() {
        double height = getPost3_img().getSize().height;
        double width = getPost3_img().getSize().width;
        double aspectRatio = height / width;
        return aspectRatio;
    }

    public double getImg4AspectRatio() {
        double height = getPost4_img().getSize().height;
        double width = getPost4_img().getSize().width;
        double aspectRatio = height / width;
        return aspectRatio;
    }

    public WebElement getButton_previous_page() {
        return button_previous_page;
    }

    public WebElement getButton_next_page() {
        return button_next_page;
    }


    public String getLogin() {
        String login = button_hello.getText().substring(7);
        return login;
    }

    public WebElement getError_code() {
        return error_code;
    }

    public WebElement getError_message() {
        return error_message;
    }

    public String getMessage() {
        return message.getText();
    }

    public WEB.TestPage change_order() {
        button_order.click();
        return this;
    }
}
