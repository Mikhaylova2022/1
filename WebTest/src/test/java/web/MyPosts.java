package web;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Блог с моими постами")
public class MyPosts {
    WebDriver driver;
    WEB.TestPage testPage;

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }


    @BeforeEach
    void newDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        driver = new ChromeDriver(options);

//        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));


        driver.get("https://test-stand.gb.ru/login");

        testPage = new WEB.TestPage(driver);

        testPage.loginUser();
    }

    @AfterEach
    void quit() {
        driver.quit();
    }

    @SneakyThrows
    @Test
    @DisplayName("Авторизация")
    public void authorizationTest() {
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/", driver.getCurrentUrl());
    }


    @Test
    @DisplayName("Наличие постов")
    void postTest() {
        assertTrue(testPage.getPost1().isDisplayed());
        assertTrue(testPage.getPost2().isDisplayed());
        assertTrue(testPage.getPost3().isDisplayed());
        assertTrue(testPage.getPost4().isDisplayed());
    }


    @SneakyThrows
    @Test
    @DisplayName("Следующая страница")
    public void nextPageTest() {
        testPage.nextPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=2", driver.getCurrentUrl());
        testPage.nextPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=3", driver.getCurrentUrl());
    }
    @SneakyThrows
    @Test
    @DisplayName("Предыдущая страница")
    public void previousPageTest() {
        Thread.sleep(3000);
        driver.get("https://test-stand.gb.ru/?page=3");
        Thread.sleep(3000);
        testPage.previousPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=2", driver.getCurrentUrl());
        Thread.sleep(3000);
        testPage.previousPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=1", driver.getCurrentUrl());
    }

    @SneakyThrows
    @Test
    @DisplayName("Предыдущая страница в начале")
    public void firstPageTest() {
        testPage.previousPage();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/", driver.getCurrentUrl());
    }



    @SneakyThrows
    @Test
    @DisplayName("Следующая  страница в конце")
    public void lastPageTest() {
        Thread.sleep(3000);
        driver.get("https://test-stand.gb.ru/?page=3");
        Thread.sleep(3000);
        testPage.nextPage();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/?page=3", driver.getCurrentUrl());
    }

    @Test
    @DisplayName("Наличие заголовков постов")
    public void postsTitleTest() {
        assertTrue(testPage.getPost1_title().isDisplayed());
        assertTrue(testPage.getPost2_title().isDisplayed());
        assertTrue(testPage.getPost3_title().isDisplayed());
        assertTrue(testPage.getPost4_title().isDisplayed());
    }

    @Test
    @DisplayName("Наличие описания постов")
    public void postsDescriptionTest() {
        assertTrue(testPage.getPost1_description().isDisplayed());
        assertTrue(testPage.getPost2_description().isDisplayed());
        assertTrue(testPage.getPost3_description().isDisplayed());
        assertTrue(testPage.getPost4_description().isDisplayed());
    }

    @Test
    @DisplayName("Наличие изоброжений постов")
    public void postsImgTest() {
        assertTrue(testPage.getPost1_img().isDisplayed());
        assertTrue(testPage.getPost2_img().isDisplayed());
        assertTrue(testPage.getPost3_img().isDisplayed());
        assertTrue(testPage.getPost4_img().isDisplayed());
    }

    @SneakyThrows
    @Test
    @DisplayName("Заглушка при отсутствии изображания")
    void imgPlaceholderTest() {
        testPage.nextPage();
        Thread.sleep(2000);
        assertTrue(testPage.getPost1_img().isDisplayed());
        assertEquals("https://test-stand.gb.ru/placeholder/800x600.gif", testPage.getPost1_img_src());
    }


    @SneakyThrows
    @Test
    @DisplayName("Сортировка по-умолчанию")
    void orderDefaultTest() {
        Thread.sleep(2000);
        driver.get("https://test-stand.gb.ru/?sort=createdAt&order=ASC");
        String firstImgSrc = testPage.getPost1_img_src();
        testPage.clickHome();
        Thread.sleep(2000);
        assertEquals(firstImgSrc, testPage.getPost1_img_src());
    }

    @SneakyThrows
    @Test
    @DisplayName("Переход на чужие посты")
    public void NotMyPostsTest() {
        testPage.switchToNotMtPosts();
        Thread.sleep(2000);
        assertEquals("https://test-stand.gb.ru/?owner=notMe&sort=createdAt&order=ASC", driver.getCurrentUrl());
    }

    @Test
    @DisplayName("Кнопка Home ведет на главную страницу")
    public void homeTest() {
        testPage.clickHome();
        assertEquals("Blog", testPage.getTitle_Blog());
    }

    @DisplayName("Страница с моими постами (без постов)")
    public class MyPostsWithoutPosts {
        WebDriver driver;
        WEB.TestPage testPage;
    }
    @Test
    @DisplayName("Сообщение об отсутствии постов")
    void postsTest() {
        assertEquals("No items for your filter", testPage.getMessage());
    }

}
