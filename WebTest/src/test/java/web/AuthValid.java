package web;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Авторизация с валидными данными")
public class AuthValid {
    WebDriver driver;
    WEB.TestPage testPage;

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void newDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        driver.get("https://test-stand.gb.ru/login");

        testPage = new WEB.TestPage(driver);

    }

    @AfterEach
    void quit() {
        driver.quit();
    }

    @SneakyThrows
    @Test
    @DisplayName("User авторизация (с постами)")
    public void authorizationEstudianteTest() {
        testPage.loginUser();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/" ,driver.getCurrentUrl());
        assertEquals("estudiante", testPage.getLogin());
    }

    @SneakyThrows
    @Test
    @DisplayName("valid1 авторизация (без постов)")
    public void authorizationValid1Test() {
        testPage.loginValid1();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/" ,driver.getCurrentUrl());
        assertEquals("valid1", testPage.getLogin());
    }

    @SneakyThrows
    @Test
    @DisplayName("Мин.")
    public void authorizationMinSymbolTest() {
        testPage.loginMinSymbol();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/" ,driver.getCurrentUrl());
        assertEquals("sin", testPage.getLogin());
    }

    @SneakyThrows
    @Test
    @DisplayName("Макс.")
    public void authorizationMaxSymbolTest() {
        testPage.loginMaxSymbol();
        Thread.sleep(1000);
        assertEquals("https://test-stand.gb.ru/" ,driver.getCurrentUrl());
        assertEquals("qwertyuiopasdfghjklf", testPage.getLogin());
    }
}
