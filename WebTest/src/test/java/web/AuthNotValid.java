package web;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
@DisplayName("Авторизация с невалидными данными (проверка сообщений об ошибках)")
public class AuthNotValid {
    static ChromeDriver driver;
    WEB.TestPage testPage;

    @BeforeEach
    void NewDriver() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");
        ChromeDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        driver.get("https://test-stand.gb.ru/login");

        testPage = new WEB.TestPage(driver);

    }

    @AfterEach
    void quit() {
        driver.quit();
    }


    @Test
    @DisplayName("Неактивированный аккаунт")
    public void authorizationInactiveTest() {
        testPage.loginInactive();
        assertEquals("Ошибка. Попробуйте еще раз позже", testPage.getError_message().getText());
    }

    @Test
    @DisplayName("Русские буквы в логине (не латиница)")
    public void authorizationRusTest() {
        testPage.loginRus();
        assertEquals("Неправильный логин. Может состоять только из латинских букв и цифр, без спецсимволов", testPage.getError_message().getText());
    }

    @Test
    @DisplayName("Специальные символы в логине")
    public void authorizationSpecSymbolTest() {
        testPage.loginSpecSymbol();
        assertEquals("Неправильный логин. Может состоять только из латинских букв и цифр, без спецсимволов", testPage.getError_message().getText());
    }

    @Test
    @DisplayName("2 символа в логине (минумум 3)")
    public void authorizationlhTest() {
        testPage.loginlh();
        assertEquals("Неправильный логин. Может быть не менее 3 и не более 20 символов", testPage.getError_message().getText());
    }

    @Test
    @DisplayName("21 символ в логине (максимум 20)")
    public void authorization21Test() {
        testPage.login21();
        assertEquals("Неправильный логин. Может быть не менее 3 и не более 20 символов", testPage.getError_message().getText());
    }

    @Test
    @DisplayName("Неправильный пароль к валидному логину")
    public void authorizationestudianteNonValidTest() {
        testPage.loginestudianteNonValid();
        assertEquals("Проверьте логин и пароль", testPage.getError_message().getText());
    }


    @Test
    @DisplayName("Пустые поля логин и пароль")
    public void authorizationWithoutLoginAndPasswordTest() {
        testPage.loginWithoutLoginAndPassword();
        assertEquals("Поле не может быть пустым", testPage.getError_message().getText());
    }
}
